"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    # calculate the absolut acceleration
    absolut_acceleration = np.sqrt(x*x+y*y+z*z)
    return absolut_acceleration
    

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    
    # interpolation of the data
    time_interpolation = np.linspace(time.min(), time.max(), num=len(time), endpoint=True)
    data_interpolation = np.interp(time_interpolation, time, data)
    
    return (time_interpolation, data_interpolation)


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    # calculate the sampling rate and run the fft function
    sampling_rate = 1/np.mean(np.diff(time))
    fft_result = np.fft.fft(x)

    # calculate the frequencies
    frequencies = np.fft.fftfreq(len(x), d=1/sampling_rate)
    
    # filter for the positive values
    pos_result = fft_result[:len(fft_result)//2]
    pos_freq = frequencies[:len(frequencies)//2]
    
    # calculate the amplitude
    amplitude_fft = np.abs(pos_result)
    
    return amplitude_fft, pos_freq
    